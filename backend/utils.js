const fs = require("fs").promises;
const path = require("path");

const pathToFile = path.resolve(__dirname, "./storage/sepulcas.json");

function dataStream(object) {
  return new Promise((resolve, reject) => {
    let rawData = "";
    object.on("data", (chunk) => {
      rawData += chunk;
    });
    object.on("end", () => {
      try {
        parsedData = JSON.parse(rawData);
        resolve(parsedData);
      } catch (e) {
        reject(e);
      }
    });
  });
}

async function appendToFile(object) {
  let buffer = await fs.readFile(pathToFile);
  // console.log("before", buffer, buffer.length, buffer.toString());
  if (buffer.length < 2) buffer = "[]";
  // console.log("after", buffer, buffer.length, buffer.toString());
  const data = JSON.parse(buffer);
  data.push(object);
  fs.writeFile(pathToFile, JSON.stringify(data)).catch((err) =>
    console.error(err)
  );
}

function writeFile(array) {
  fs.writeFile(pathToFile, JSON.stringify(array)).catch((err) =>
    console.error(err)
  );
}

async function readFile() {
  try {
    return await fs.readFile(pathToFile);
  } catch (err) {
    console.error(err);
  }
}

async function removeFromFile(id) {
  const { parsedObjects, index, isEmpty } = await checkExistence(id);

  if (isEmpty) return false;
  if (index === -1) return false;

  parsedObjects.splice(index, 1);
  writeFile(parsedObjects);
  return true;
}

async function updateFile(id, updatedProperties) {
  const { parsedObjects, index, isEmpty } = await checkExistence(id);

  if (isEmpty) return false;
  if (index === -1) return false;

  parsedObjects[index] = {
    ...parsedObjects[index],
    ...updatedProperties,
  };
  writeFile(parsedObjects);
  return true;
}

const getObjectIndexByID = (objects, id) => {
  return objects.findIndex((object) => object.id === id);
};

const checkExistence = async (id) => {
  const buffer = await readFile();
  if (buffer.length === 0) return { isEmpty: true };

  const parsedObjects = JSON.parse(buffer);
  const index = getObjectIndexByID(parsedObjects, id);

  return { parsedObjects, index, isEmpty: false };
};

module.exports = {
  dataStream,
  appendToFile,
  readFile,
  writeFile,
  removeFromFile,
  updateFile,
};
