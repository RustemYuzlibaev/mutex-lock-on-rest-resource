const makeRequest = require("./utils/makeRequest");
const config = require("./config");

class Storage {
  constructor(url) {
    this.url = config.urlService;
  }

  async get(id) {
    try {
      const data = await makeRequest({ path: "/api/get" });
      return data.find((sepulca) => sepulca.id === id);
    } catch (e) {
      console.error(e);
    }
  }

  async getAll() {
    try {
      const data = await makeRequest({ path: "/api/getall" });
      return data;
    } catch (e) {
      console.error(e);
    }
  }

  store(object) {
    const objectString = JSON.stringify(object);
    makeRequest({
      method: "POST",
      path: "/api/store",
      data: objectString,
    }).catch((e) => console.error(e));
  }

  remove(id) {
    const objectString = JSON.stringify({ id });
    makeRequest({
      method: "POST",
      path: "/api/remove",
      data: objectString,
    }).catch((e) => console.error(e));
  }

  update(id, updatedProperties) {
    const objectString = JSON.stringify({ id, updatedProperties });
    makeRequest({
      method: "POST",
      path: "/api/update",
      data: objectString,
    }).catch((e) => console.error(e));
  }
}

module.exports = Storage;
