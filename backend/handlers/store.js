const { dataStream, appendToFile } = require("../utils");

module.exports = async function store(req, res, lock) {
  await lock.acquire();
  const object = await dataStream(req).catch((err) => console.log(err));
  appendToFile(object);
  res.end(JSON.stringify({ success: true }));
  lock.release();
};
