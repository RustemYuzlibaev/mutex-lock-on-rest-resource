const sepulcaLib = require("./SepulcaLib")();

// Client doesn't know anything about storage
let id = sepulcaLib.computeID();
let id2 = sepulcaLib.computeID();

// Fixtures
const sepulca = {
  id: id,
  service: "sepulca#1",
  uptime: 23,
  isOnline: true,
};

const sepulca2 = {
  id: "1",
  service: "sepulca#2",
  uptime: 18,
  isOnline: true,
};

sepulcaLib.store(sepulca);

// setTimeout(() => sepulcaLib.store(sepulca2), 1000);

sepulcaLib.getAll().then((sepulcas) => console.log(sepulcas));
sepulcaLib.get("1").then((sepulca) => console.log(sepulca));

sepulcaLib.remove("1");

sepulcaLib.update("1", {
  service: "sepulca2Update",
  isOnline: true,
  additionalOption: "http://url.com/updated",
});
