const http = require("http");
const config = require("../config");
const Lock = require("../LockOnClient");

const lock = new Lock();

/**
 * HTTP utility for handling requests.
 * Requirements: Promise based HTTP client
 *
 *
 * @example
 * makeRequest({
 *   method: "POST",
 *   path: "/api/store",
 *   data,
 * }).catch((e) => console.error(e))
 *
 *
 *
 * 1. Coming request executes immediately if queue is empty.
 * 2. Coming requests are pushed to waiting queue if queue is not empty,
 * then executed
 *
 */

function makeRequest(header) {
  return new Promise((resolve, reject) => {
    lock.hold(() => {
      const options = {
        hostname: new URL(config.urlService).hostname,
        port: 8080,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
        ...header,
      };

      const req = http.request(options, (res) => {
        dataStream(res, resolve, reject);
        lock.release();
      });

      req.on("error", (error) => {
        console.error("something", error);
      });

      if (options.method === "POST") req.write(header.data);
      req.end();
    });
  });
}

function dataStream(object, resolve, reject) {
  let rawData = "";

  object.on("data", (chunk) => {
    rawData += chunk;
  });

  object.on("end", () => {
    try {
      parsedData = JSON.parse(rawData);
      resolve(parsedData);
    } catch (e) {
      reject(e.message);
    }
  });
}

module.exports = makeRequest;
