const crypto = require("crypto");
const Storage = require("./Storage");

/**
 * Sepulca library for store, get, update and remove operations.
 * Requirements: none
 *
 * @examples
 * const id = sepulcaLib.computeID();
 * sepulcaLib.get(id).then((sepulca) => console.log(sepulca));
 * sepulcaLib.remove(id);
 *
 */

class SepulcaLib {
  constructor(storageAPI) {
    this.storageAPI = storageAPI;
  }

  computeID() {
    return crypto.randomBytes(32).toString("hex");
  }

  get(id) {
    return this.storageAPI.get(id);
  }

  getAll() {
    return this.storageAPI.getAll();
  }

  store(sepulca) {
    this.storageAPI.store(sepulca);
  }

  remove(id) {
    this.storageAPI.remove(id);
  }

  update(id, updatedProperties) {
    this.storageAPI.update(id, updatedProperties);
  }
}

/**
 * We can easily switch between storage systems and client will not
 * know anything about it. (Assuming the interface is the same)
 */
module.exports = () => new SepulcaLib(new Storage());
