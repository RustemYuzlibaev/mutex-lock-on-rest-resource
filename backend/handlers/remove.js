const { dataStream, removeFromFile } = require("../utils");

module.exports = async function remove(req, res, lock) {
  await lock.acquire();
  const { id } = await dataStream(req).catch((err) => console.log(err));
  const result = await removeFromFile(id);
  res.end(JSON.stringify({ success: result }));
  lock.release();
};
