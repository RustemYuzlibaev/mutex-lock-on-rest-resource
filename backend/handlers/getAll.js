const { readFile } = require("../utils");

module.exports = async function getAll(req, res, lock) {
  await lock.acquire();
  const content = await readFile();
  res.end(content.toString());
  lock.release();
};
