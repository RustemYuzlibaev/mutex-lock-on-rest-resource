const { dataStream, updateFile } = require("../utils");

module.exports = async function update(req, res, lock) {
  await lock.acquire();
  const { id, updatedProperties } = await dataStream(req).catch((err) =>
    console.error(err)
  );
  const result = await updateFile(id, updatedProperties);
  res.end(JSON.stringify({ success: result }));
  lock.release();
};
