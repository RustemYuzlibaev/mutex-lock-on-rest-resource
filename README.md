### Sepulca Project

It provides consistent access to REST resource by means of **mutex lock**.



#### Client / Consumer:

`client.js:` playground to call APIs.

`SepulcaLib.js:` library capable of working with different storage APIs.

`Storage.js:` class providing access to storage.

`utils/makeRequest.js:` is function making API to backend server.

`config.js:` configuration file.

`LockOnClient.js:` implementation of locking mechanism.



#### Backend server:

`handlers/:` directory for REST API handlers.

`storage/:` directory for emulating storage resource.

`server.js`: server.

`utils.js:` utility to call file system API.

`LockOnServer.js:` asynchronous locking on file system calls. 





