const http = require("http");

const Lock = require("./LockOnServer");

const get = require("./handlers/get");
const getAll = require("./handlers/getAll");
const remove = require("./handlers/remove");
const store = require("./handlers/store");
const update = require("./handlers/update");

const routes = {
  "/api/get": get,
  "/api/getall": getAll,
  "/api/store": store,
  "/api/remove": remove,
  "/api/update": update,
  "/notFound": `¯\_(ツ)_/¯`,
};

const lock = new Lock();

const server = http.createServer((req, res) => {
  const path = req.url;
  let route =
    typeof routes[path] !== "undefined" ? routes[path] : routes["/notFound"];

  route(req, res, lock);
});

server.listen(8080, () => "Server is running on port 8080");
